CREATE TABLE clientes(id NUMBER(5) PRIMARY KEY,nombre VARCHAR2(80) NOT NULL,identificacion VARCHAR2(16) NOT NULL);
/
CREATE TABLE pedidos(id NUMBER(5) PRIMARY KEY,cliente_id NUMBER(5) NOT NULL,precio_total NUMBER(20) NOT NULL,CONSTRAINT fk_pedidos_cliente FOREIGN KEY(cliente_id) REFERENCES clientes(id));
/
CREATE TABLE productos(id NUMBER(5) PRIMARY KEY,nombre VARCHAR2(30) NOT NULL,precio NUMBER(18) NOT NULL);
/
CREATE TABLE productos_pedidos(id NUMBER(5) PRIMARY KEY,producto_id NUMBER(5) NOT NULL,pedido_id NUMBER(5) NOT NULL,CONSTRAINT fk_productos_pedidos_producto FOREIGN KEY(producto_id) REFERENCES productos(id),CONSTRAINT fk_productos_pedidos_pedido FOREIGN KEY(pedido_id) REFERENCES pedidos(id));
/
CREATE SEQUENCE clientes_secuencia INCREMENT BY 1 NOCACHE NOCYCLE;
/
CREATE SEQUENCE productos_secuencia INCREMENT BY 1 NOCACHE NOCYCLE;
/
CREATE SEQUENCE pedidos_secuencia INCREMENT BY 1 NOCACHE NOCYCLE;
/
CREATE SEQUENCE productos_pedidos_secuencia INCREMENT BY 1 NOCACHE NOCYCLE;
/
CREATE OR REPLACE PACKAGE pkg_manejo_productos IS
	PROCEDURE depurar_productos(p_pedido_id IN pedidos.id%TYPE,p_producto_id IN productos.id%TYPE);
	FUNCTION cantidad_productos(p_pedido_id IN pedidos.id%TYPE)RETURN NUMBER;
END;
/
CREATE OR REPLACE PACKAGE BODY pkg_manejo_productos IS
	PROCEDURE depurar_productos(p_pedido_id IN pedidos.id%TYPE,p_producto_id IN productos.id%TYPE) IS
	BEGIN
		DELETE productos_pedidos WHERE pedido_id=p_pedido_id AND producto_id=p_producto_id;
	END;
	FUNCTION cantidad_productos(p_pedido_id IN pedidos.id%TYPE)RETURN NUMBER IS
		resultado NUMBER(20);
	BEGIN
		SELECT COUNT(*) INTO resultado FROM productos_pedidos WHERE pedido_id=p_pedido_id;
		RETURN resultado;
	END;
END;
/
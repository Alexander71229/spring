DROP PACKAGE BODY pkg_manejo_productos;
DROP PACKAGE pkg_manejo_productos;
DROP SEQUENCE clientes_secuencia;
DROP SEQUENCE productos_secuencia;
DROP SEQUENCE pedidos_secuencia;
DROP SEQUENCE productos_pedidos_secuencia;
DROP TABLE clientes CASCADE CONSTRAINTS PURGE;
DROP TABLE pedidos CASCADE CONSTRAINTS PURGE;
DROP TABLE productos CASCADE CONSTRAINTS PURGE;
DROP TABLE productos_pedidos CASCADE CONSTRAINTS PURGE;


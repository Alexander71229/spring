package com.example.servicio.controller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Optional;
import com.example.servicio.domain.Cliente;
import com.example.servicio.core.IServicioClientes;
@RestController
@RequestMapping("/api")
public class ClientesControlador implements IClientesControlador{
	@Autowired
	IServicioClientes servicioClientes;
	@Override
	@RequestMapping(value={"/clientes"},method=RequestMethod.GET)
	public Iterable<Cliente>listarClientes(){
		return servicioClientes.listarClientes();
	}
	@Override
	@RequestMapping(value={"/clientes/{id}"},method=RequestMethod.GET)
	public Optional<Cliente>obtenerCliente(@PathVariable Long id){
		return servicioClientes.obtenerCliente(id);
	}
	@Override
	@RequestMapping(value={"/clientes/add"},method=RequestMethod.POST)
	public Cliente agregarCliente(@RequestBody Cliente cliente){
		return servicioClientes.agregarCliente(cliente);
	}
	@Override
	@RequestMapping(value={"/clientes/delete/{id}"},method=RequestMethod.DELETE)
	public boolean eliminarCliente(@PathVariable Long id){
		return servicioClientes.eliminarCliente(id);
	}
	@Override
	@RequestMapping(value={"/clientes/update"},method=RequestMethod.PUT)
	public boolean actualizarCliente(@RequestBody Cliente cliente){
		return servicioClientes.actualizarCliente(cliente);
	}
}
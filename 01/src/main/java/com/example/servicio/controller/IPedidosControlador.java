package com.example.servicio.controller;
import java.util.Optional;
import com.example.servicio.domain.Pedido;
public interface IPedidosControlador{
	public Iterable<Pedido>listarPedidos();
	public Optional<Pedido>obtenerPedido(Long id);
	public Pedido agregarPedido(Pedido pedido);
	public boolean eliminarPedido(Long id);
	public boolean actualizarPedido(Pedido pedido);
}
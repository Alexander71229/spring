package com.example.servicio.controller;
import java.util.Optional;
import com.example.servicio.domain.Producto;
public interface IProductosControlador{
	public Iterable<Producto>listarProductos();
	public Optional<Producto>obtenerProducto(Long id);
	public Producto agregarProducto(Producto producto);
	public boolean eliminarProducto(Long id);
	public boolean actualizarProducto(Producto producto);
}
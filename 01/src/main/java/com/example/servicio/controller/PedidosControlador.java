package com.example.servicio.controller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Optional;
import com.example.servicio.domain.Pedido;
import com.example.servicio.core.IServicioPedidos;
@RestController
@RequestMapping("/api")
public class PedidosControlador implements IPedidosControlador{
	@Autowired
	IServicioPedidos servicioPedidos;
	@Override
	@RequestMapping(value={"/pedidos"},method=RequestMethod.GET)
	public Iterable<Pedido>listarPedidos(){
		return servicioPedidos.listarPedidos();
	}
	@Override
	@RequestMapping(value={"/pedidos/{id}"},method=RequestMethod.GET)
	public Optional<Pedido>obtenerPedido(@PathVariable Long id){
		return servicioPedidos.obtenerPedido(id);
	}
	@Override
	@RequestMapping(value={"/pedidos/add"},method=RequestMethod.POST)
	public Pedido agregarPedido(@RequestBody Pedido pedido){
		return servicioPedidos.agregarPedido(pedido);
	}
	@Override
	@RequestMapping(value={"/pedidos/delete/{id}"},method=RequestMethod.DELETE)
	public boolean eliminarPedido(@PathVariable Long id){
		return servicioPedidos.eliminarPedido(id);
	}
	@Override
	@RequestMapping(value={"/pedidos/update"},method=RequestMethod.PUT)
	public boolean actualizarPedido(@RequestBody Pedido pedido){
		return servicioPedidos.actualizarPedido(pedido);
	}
}
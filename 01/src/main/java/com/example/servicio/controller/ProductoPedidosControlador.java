package com.example.servicio.controller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Optional;
import com.example.servicio.domain.ProductoPedido;
import com.example.servicio.core.IServicioProductoPedidos;
@RestController
@RequestMapping("/api")
public class ProductoPedidosControlador implements IProductoPedidosControlador{
	@Autowired
	IServicioProductoPedidos servicioProductoPedidos;
	@Override
	@RequestMapping(value={"/productoPedidos"},method=RequestMethod.GET)
	public Iterable<ProductoPedido>listarProductoPedidos(){
		return servicioProductoPedidos.listarProductoPedidos();
	}
	@Override
	@RequestMapping(value={"/productoPedidos/{id}"},method=RequestMethod.GET)
	public Optional<ProductoPedido>obtenerProductoPedido(@PathVariable Long id){
		return servicioProductoPedidos.obtenerProductoPedido(id);
	}
	@Override
	@RequestMapping(value={"/productoPedidos/add"},method=RequestMethod.POST)
	public ProductoPedido agregarProductoPedido(@RequestBody ProductoPedido productoPedido){
		return servicioProductoPedidos.agregarProductoPedido(productoPedido);
	}
	@Override
	@RequestMapping(value={"/productoPedidos/delete/{id}"},method=RequestMethod.DELETE)
	public boolean eliminarProductoPedido(@PathVariable Long id){
		return servicioProductoPedidos.eliminarProductoPedido(id);
	}
	@Override
	@RequestMapping(value={"/productoPedidos/update"},method=RequestMethod.PUT)
	public boolean actualizarProductoPedido(@RequestBody ProductoPedido productoPedido){
		return servicioProductoPedidos.actualizarProductoPedido(productoPedido);
	}
	@Override
	@RequestMapping(value={"/productoPedidos/depurarProductos/{pedido_id}/{producto_id}"},method=RequestMethod.PUT)
	public boolean depurarProductos(@PathVariable Long pedido_id,@PathVariable Long producto_id){
		return servicioProductoPedidos.depurarProductos(pedido_id,producto_id);
	}
	@Override
	@RequestMapping(value={"/productoPedidos/cantidadProductos/{pedido_id}"},method=RequestMethod.GET)
	public int cantidadProductos(@PathVariable Long pedido_id){
		return servicioProductoPedidos.cantidadProductos(pedido_id);
	}
}
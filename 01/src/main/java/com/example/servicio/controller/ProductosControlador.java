package com.example.servicio.controller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Optional;
import com.example.servicio.domain.Producto;
import com.example.servicio.core.IServicioProductos;
@RestController
@RequestMapping("/api")
public class ProductosControlador implements IProductosControlador{
	@Autowired
	IServicioProductos servicioProductos;
	@Override
	@RequestMapping(value={"/productos"},method=RequestMethod.GET)
	public Iterable<Producto>listarProductos(){
		return servicioProductos.listarProductos();
	}
	@Override
	@RequestMapping(value={"/productos/{id}"},method=RequestMethod.GET)
	public Optional<Producto>obtenerProducto(@PathVariable Long id){
		return servicioProductos.obtenerProducto(id);
	}
	@Override
	@RequestMapping(value={"/productos/add"},method=RequestMethod.POST)
	public Producto agregarProducto(@RequestBody Producto producto){
		return servicioProductos.agregarProducto(producto);
	}
	@Override
	@RequestMapping(value={"/productos/delete/{id}"},method=RequestMethod.DELETE)
	public boolean eliminarProducto(@PathVariable Long id){
		return servicioProductos.eliminarProducto(id);
	}
	@Override
	@RequestMapping(value={"/productos/update"},method=RequestMethod.PUT)
	public boolean actualizarProducto(@RequestBody Producto producto){
		return servicioProductos.actualizarProducto(producto);
	}
}
package com.example.servicio.core;
import java.util.Optional;
import com.example.servicio.domain.Cliente;
public interface IServicioClientes{
	public Iterable<Cliente>listarClientes();
	public Optional<Cliente>obtenerCliente(Long id);
	public Cliente agregarCliente(Cliente cliente);
	public boolean eliminarCliente(Long id);
	public boolean actualizarCliente(Cliente cliente);
}
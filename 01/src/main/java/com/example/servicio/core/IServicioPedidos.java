package com.example.servicio.core;
import java.util.Optional;
import com.example.servicio.domain.Pedido;
public interface IServicioPedidos{
	public Iterable<Pedido>listarPedidos();
	public Optional<Pedido>obtenerPedido(Long id);
	public Pedido agregarPedido(Pedido pedido);
	public boolean eliminarPedido(Long id);
	public boolean actualizarPedido(Pedido pedido);
}
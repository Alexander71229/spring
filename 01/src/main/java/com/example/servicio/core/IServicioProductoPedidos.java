package com.example.servicio.core;
import java.util.Optional;
import com.example.servicio.domain.ProductoPedido;
public interface IServicioProductoPedidos{
	public Iterable<ProductoPedido>listarProductoPedidos();
	public Optional<ProductoPedido>obtenerProductoPedido(Long id);
	public ProductoPedido agregarProductoPedido(ProductoPedido productoPedido);
	public boolean eliminarProductoPedido(Long id);
	public boolean actualizarProductoPedido(ProductoPedido productoPedido);
	public boolean depurarProductos(Long pedido_id,Long producto_id);
	public int cantidadProductos(Long pedido_id);
}
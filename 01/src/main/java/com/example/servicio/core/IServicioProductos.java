package com.example.servicio.core;
import java.util.Optional;
import com.example.servicio.domain.Producto;
public interface IServicioProductos{
	public Iterable<Producto>listarProductos();
	public Optional<Producto>obtenerProducto(Long id);
	public Producto agregarProducto(Producto Producto);
	public boolean eliminarProducto(Long id);
	public boolean actualizarProducto(Producto producto);
}
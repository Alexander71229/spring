package com.example.servicio.core;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.servicio.domain.Cliente;
import com.example.servicio.dao.IClientesRepositorio;
@Service
public class ServicioClientes implements IServicioClientes{
	@Autowired
	IClientesRepositorio clientesRepositorio;
	@Override
	public Iterable<Cliente>listarClientes(){
		return clientesRepositorio.findAll();
	}
	@Override
	public Optional<Cliente>obtenerCliente(Long id){
		return clientesRepositorio.findById(id);
	}
	@Override
	public Cliente agregarCliente(Cliente cliente){
		return clientesRepositorio.save(cliente);
	}
	@Override
	public boolean eliminarCliente(Long id){
		if(clientesRepositorio.findById(id).isPresent()){
			clientesRepositorio.deleteById(id);
			return true;
		}
		return false;
	}
	@Override
	public boolean actualizarCliente(Cliente cliente){
		if(clientesRepositorio.findById(cliente.getId()).isPresent()){
			clientesRepositorio.save(cliente);
			return true;
		}
		return false;
	}
}
package com.example.servicio.core;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.servicio.domain.Pedido;
import com.example.servicio.dao.IPedidosRepositorio;
@Service
public class ServicioPedidos implements IServicioPedidos{
	@Autowired
	IPedidosRepositorio pedidosRepositorio;
	@Override
	public Iterable<Pedido>listarPedidos(){
		return pedidosRepositorio.findAll();
	}
	@Override
	public Optional<Pedido>obtenerPedido(Long id){
		return pedidosRepositorio.findById(id);
	}
	@Override
	public Pedido agregarPedido(Pedido pedido){
		return pedidosRepositorio.save(pedido);
	}
	@Override
	public boolean eliminarPedido(Long id){
		if(pedidosRepositorio.findById(id).isPresent()){
			pedidosRepositorio.deleteById(id);
			return true;
		}
		return false;
	}
	@Override
	public boolean actualizarPedido(Pedido pedido){
		if(pedidosRepositorio.findById(pedido.getId()).isPresent()){
			pedidosRepositorio.save(pedido);
			return true;
		}
		return false;
	}
}
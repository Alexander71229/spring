package com.example.servicio.core;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.servicio.domain.ProductoPedido;
import com.example.servicio.dao.IProductosPedidosRepositorio;
import com.example.servicio.dao.IRepositorio;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
@Service
public class ServicioProductoPedidos implements IServicioProductoPedidos{
	@Autowired
	IProductosPedidosRepositorio productoPedidosRepositorio;
	@Autowired
	IRepositorio repositorio;
	@Override
	public Iterable<ProductoPedido>listarProductoPedidos(){
		return productoPedidosRepositorio.findAll();
	}
	@Override
	public Optional<ProductoPedido>obtenerProductoPedido(Long id){
		return productoPedidosRepositorio.findById(id);
	}
	@Override
	public ProductoPedido agregarProductoPedido(ProductoPedido productoPedido){
		return productoPedidosRepositorio.save(productoPedido);
	}
	@Override
	public boolean eliminarProductoPedido(Long id){
		if(productoPedidosRepositorio.findById(id).isPresent()){
			productoPedidosRepositorio.deleteById(id);
			return true;
		}
		return false;
	}
	@Override
	public boolean actualizarProductoPedido(ProductoPedido productoPedido){
		if(productoPedidosRepositorio.findById(productoPedido.getId()).isPresent()){
			productoPedidosRepositorio.save(productoPedido);
			return true;
		}
		return false;
	}
	@Override
	public boolean depurarProductos(Long pedido_id,Long producto_id){
		Map<String,Object>parametros=new HashMap<String,Object>();
		parametros.put("p_pedido_id",pedido_id);
		parametros.put("p_producto_id",producto_id);
		Map<String,Object>salida=repositorio.ejecutarProcedimientoAlmacenado("pkg_manejo_productos","depurar_productos",parametros);
		return true;
	}
	@Override
	public int cantidadProductos(Long pedido_id){
		Map<String,Object>parametros=new HashMap<String,Object>();
		parametros.put("p_pedido_id",pedido_id);
		return(repositorio.ejecutarFuncion("pkg_manejo_productos","cantidad_productos",BigDecimal.class,parametros)).intValue();
	}
}
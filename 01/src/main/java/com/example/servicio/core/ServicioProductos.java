package com.example.servicio.core;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.servicio.domain.Producto;
import com.example.servicio.dao.IProductosRepositorio;
@Service
public class ServicioProductos implements IServicioProductos{
	@Autowired
	IProductosRepositorio productosRepositorio;
	@Override
	public Iterable<Producto>listarProductos(){
		return productosRepositorio.findAll();
	}
	@Override
	public Optional<Producto>obtenerProducto(Long id){
		return productosRepositorio.findById(id);
	}
	@Override
	public Producto agregarProducto(Producto producto){
		return productosRepositorio.save(producto);
	}
	@Override
	public boolean eliminarProducto(Long id){
		if(productosRepositorio.findById(id).isPresent()){
			productosRepositorio.deleteById(id);
			return true;
		}
		return false;
	}
	@Override
	public boolean actualizarProducto(Producto producto){
		if(productosRepositorio.findById(producto.getId()).isPresent()){
			productosRepositorio.save(producto);
			return true;
		}
		return false;
	}
}
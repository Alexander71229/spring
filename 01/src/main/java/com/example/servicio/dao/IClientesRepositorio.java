package com.example.servicio.dao;
import org.springframework.data.repository.CrudRepository;
import com.example.servicio.domain.Cliente;
public interface IClientesRepositorio extends CrudRepository<Cliente,Long>{
}
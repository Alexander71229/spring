package com.example.servicio.dao;
import org.springframework.data.repository.CrudRepository;
import com.example.servicio.domain.Pedido;
public interface IPedidosRepositorio extends CrudRepository<Pedido,Long>{
}
package com.example.servicio.dao;
import org.springframework.data.repository.CrudRepository;
import com.example.servicio.domain.ProductoPedido;
public interface IProductosPedidosRepositorio extends CrudRepository<ProductoPedido,Long>{
}
package com.example.servicio.dao;
import org.springframework.data.repository.CrudRepository;
import com.example.servicio.domain.Producto;
public interface IProductosRepositorio extends CrudRepository<Producto,Long>{
}
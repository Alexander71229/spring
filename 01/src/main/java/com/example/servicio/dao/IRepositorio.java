package com.example.servicio.dao;
import java.util.List;
import java.util.Map;
import java.sql.ResultSet;
import org.springframework.stereotype.Repository;
@Repository
public interface IRepositorio{
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada);
	public <T>T ejecutarFuncion(String paquete,String nombreFuncion,Class<T>clase,Map<String,Object>parametrosEntrada);
}
package com.example.servicio.dao;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
@Repository
public class Repositorio implements IRepositorio{
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	@Autowired
	public void setDataSource(final DataSource dataSource){
		this.dataSource=dataSource;
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	public Map<String,Object>ejecutarProcedimientoAlmacenado(String paquete,String nombreProcedimiento,Map<String,Object>parametrosEntrada){
		return new SimpleJdbcCall(dataSource).withCatalogName(paquete).withProcedureName(nombreProcedimiento).execute(parametrosEntrada);
	}
	public <T>T ejecutarFuncion(String paquete,String nombreFuncion,Class<T>clase,Map<String,Object>parametrosEntrada){
		return new SimpleJdbcCall(dataSource).withCatalogName(paquete).withFunctionName(nombreFuncion).executeFunction(clase,parametrosEntrada);
	}
}
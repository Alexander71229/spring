package com.example.servicio.domain;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
@Entity
@Table(name="CLIENTES")
public class Cliente{
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="clientes_secuencia")
	@SequenceGenerator(name="clientes_secuencia",sequenceName="CLIENTES_SECUENCIA",allocationSize=1)
	@Column(name="ID")
	private Long id;
	@Column(name="NOMBRE")
	private String nombre;
	@Column(name="IDENTIFICACION")
	private String identificacion;
	public void setId(Long id){
		this.id=id;
	}
	public Long getId(){
		return this.id;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getNombre(){
		return this.nombre;
	}
	public void setIdentificacion(String identificacion){
		this.identificacion=identificacion;
	}
	public String getIdentificacion(){
		return this.identificacion;
	}
}
package com.example.servicio.domain;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;
@Entity
@Table(name="PEDIDOS")
public class Pedido{
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="pedidos_secuencia")
	@SequenceGenerator(name="pedidos_secuencia",sequenceName="PEDIDOS_SECUENCIA",allocationSize=1)
	@Column(name="ID")
	private Long id;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="CLIENTE_ID")
	private Cliente cliente;
	@Column(name="PRECIO_TOTAL")
	private String precioTotal;
	public void setId(Long id){
		this.id=id;
	}
	public Long getId(){
		return this.id;
	}
	public void setCliente(Cliente cliente){
		this.cliente=cliente;
	}
	public Cliente getCliente(){
		return this.cliente;
	}
	public void setPrecioTotal(String precioTotal){
		this.precioTotal=precioTotal;
	}
	public String getPrecioTotal(){
		return this.precioTotal;
	}
}
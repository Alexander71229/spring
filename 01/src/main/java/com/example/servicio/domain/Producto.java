package com.example.servicio.domain;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
@Entity
@Table(name="PRODUCTOS")
public class Producto{
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="productos_secuencia")
	@SequenceGenerator(name="productos_secuencia",sequenceName="PRODUCTOS_SECUENCIA",allocationSize=1)
	@Column(name="ID")
	private Long id;
	@Column(name="NOMBRE")
	private String nombre;
	@Column(name="PRECIO")
	private String precio;
	public void setId(Long id){
		this.id=id;
	}
	public Long getId(){
		return this.id;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getNombre(){
		return this.nombre;
	}
	public void setPrecio(String precio){
		this.precio=precio;
	}
	public String getPrecio(){
		return this.precio;
	}
}
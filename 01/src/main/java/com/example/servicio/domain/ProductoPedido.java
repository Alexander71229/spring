package com.example.servicio.domain;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;
@Entity
@Table(name="PRODUCTOS_PEDIDOS")
public class ProductoPedido{
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="productos_pedidos_secuencia")
	@SequenceGenerator(name="productos_pedidos_secuencia",sequenceName="PRODUCTOS_PEDIDOS_SECUENCIA",allocationSize=1)
	@Column(name="ID")
	private Long id;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="PRODUCTO_ID")
	private Producto producto;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="PEDIDO_ID")
	private Pedido pedido;
	public void setId(Long id){
		this.id=id;
	}
	public Long getId(){
		return this.id;
	}
	public void setProducto(Producto producto){
		this.producto=producto;
	}
	public Producto getProducto(){
		return this.producto;
	}
	public void setPedido(Pedido pedido){
		this.pedido=pedido;
	}
	public Pedido getPedido(){
		return this.pedido;
	}
}
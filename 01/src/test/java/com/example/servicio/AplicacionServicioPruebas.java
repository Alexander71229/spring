package com.example.servicio;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import com.google.gson.Gson;
import static org.hamcrest.CoreMatchers.hasItem;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.is;
@SpringBootTest
@AutoConfigureMockMvc
class AplicacionServicioPruebas{
	@Autowired
	private MockMvc mockMvc;
	@Test
	public void test01RutaPositiva()throws Exception{
		String json="{\"id\":1,\"nombre\":\"Alexander Molina Grisales\",\"identificacion\":\"71229276\"}";
		this.mockMvc.perform(post("/api/clientes/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		this.mockMvc.perform(get("/api/clientes")).andExpect(status().isOk()).andExpect(jsonPath("$[?(@.id==\"1\")]").isNotEmpty());
		this.mockMvc.perform(get("/api/clientes/1")).andExpect(status().isOk()).andExpect(jsonPath("$.id",is(1)));
		json="{\"id\":1,\"nombre\":\"Alexander Molina Grisales\",\"identificacion\":\"71229278\"}";
		this.mockMvc.perform(put("/api/clientes/update").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(get("/api/clientes")).andExpect(status().isOk()).andExpect(jsonPath("$[?(@.identificacion==\"71229278\")]").isNotEmpty());
		json="{\"id\":1,\"nombre\":\"silla\",\"precio\":\"300000\"}";
		this.mockMvc.perform(post("/api/productos/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		this.mockMvc.perform(get("/api/productos")).andExpect(status().isOk()).andExpect(jsonPath("$[?(@.id==\"1\")]").isNotEmpty());
		this.mockMvc.perform(get("/api/productos/1")).andExpect(status().isOk()).andExpect(jsonPath("$.id",is(1)));
		json="{\"id\":2,\"nombre\":\"mesa\",\"precio\":\"500000\"}";
		this.mockMvc.perform(post("/api/productos/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		this.mockMvc.perform(get("/api/productos")).andExpect(status().isOk()).andExpect(jsonPath("$[?(@.id==\"2\")]").isNotEmpty());
		this.mockMvc.perform(get("/api/productos/2")).andExpect(status().isOk()).andExpect(jsonPath("$.id",is(2)));
		json="{\"id\":1,\"nombre\":\"silla\",\"precio\":\"320000\"}";
		this.mockMvc.perform(put("/api/productos/update").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(get("/api/productos/1")).andExpect(status().isOk()).andExpect(jsonPath("$.id",is(1))).andExpect(jsonPath("$.precio",is("320000")));
		json="{\"id\":1,\"cliente\":{\"id\":1},\"precioTotal\":\"0\"}";
		this.mockMvc.perform(post("/api/pedidos/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		this.mockMvc.perform(get("/api/pedidos")).andExpect(status().isOk()).andExpect(jsonPath("$[?(@.id==\"1\")]").isNotEmpty());
		this.mockMvc.perform(get("/api/pedidos/1")).andExpect(status().isOk()).andExpect(jsonPath("$.id",is(1)));
		json="{\"id\":1,\"cliente\":{\"id\":1},\"precioTotal\":\"50000\"}";
		this.mockMvc.perform(put("/api/pedidos/update").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(get("/api/pedidos/1")).andExpect(status().isOk()).andExpect(jsonPath("$.id",is(1))).andExpect(jsonPath("$.precioTotal",is("50000")));
		json="{\"id\":1,\"producto\":{\"id\":1},\"pedido\":{\"id\":1}}";
		this.mockMvc.perform(post("/api/productoPedidos/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		this.mockMvc.perform(get("/api/productoPedidos")).andExpect(status().isOk()).andExpect(jsonPath("$[?(@.id==\"1\")]").isNotEmpty());
		this.mockMvc.perform(get("/api/productoPedidos/1")).andExpect(status().isOk()).andExpect(jsonPath("$.id",is(1)));
		json="{\"id\":1,\"producto\":{\"id\":2},\"pedido\":{\"id\":1}}";
		this.mockMvc.perform(put("/api/productoPedidos/update").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(delete("/api/productoPedidos/delete/1")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(get("/api/productoPedidos")).andExpect(status().isOk()).andExpect(jsonPath("$[?(@.id==\"1\")]").isEmpty());
		this.mockMvc.perform(delete("/api/pedidos/delete/1")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(get("/api/pedidos")).andExpect(status().isOk()).andExpect(jsonPath("$[?(@.id==\"1\")]").isEmpty());
		this.mockMvc.perform(delete("/api/productos/delete/1")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(get("/api/productos")).andExpect(status().isOk()).andExpect(jsonPath("$[?(@.id==\"1\")]").isEmpty());
		this.mockMvc.perform(delete("/api/productos/delete/2")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(get("/api/productos")).andExpect(status().isOk()).andExpect(jsonPath("$[?(@.id==\"2\")]").isEmpty());
		this.mockMvc.perform(delete("/api/clientes/delete/1")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(get("/api/clientes")).andExpect(status().isOk()).andExpect(jsonPath("$[?(@.id==\"1\")]").isEmpty());
	}
	@Test
	public void test02DepurarProductos()throws Exception{
		String json="{\"id\":2,\"nombre\":\"Alexander Molina Grisales\",\"identificacion\":\"71229276\"}";
		this.mockMvc.perform(post("/api/clientes/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		json="{\"id\":3,\"nombre\":\"silla\",\"precio\":\"300000\"}";
		this.mockMvc.perform(post("/api/productos/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		json="{\"id\":2,\"cliente\":{\"id\":2},\"precioTotal\":\"0\"}";
		this.mockMvc.perform(post("/api/pedidos/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		json="{\"id\":2,\"producto\":{\"id\":3},\"pedido\":{\"id\":2}}";
		this.mockMvc.perform(post("/api/productoPedidos/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		this.mockMvc.perform(put("/api/productoPedidos/depurarProductos/2/2")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(delete("/api/productoPedidos/delete/2")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(delete("/api/pedidos/delete/2")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(delete("/api/productos/delete/3")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(delete("/api/clientes/delete/2")).andExpect(status().isOk()).andExpect(content().string("true"));
	}
	@Test
	public void test03CantidadProductos()throws Exception{
		String json="{\"id\":3,\"nombre\":\"Alexander Molina Grisales\",\"identificacion\":\"71229276\"}";
		this.mockMvc.perform(post("/api/clientes/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		json="{\"id\":4,\"nombre\":\"silla\",\"precio\":\"300000\"}";
		this.mockMvc.perform(post("/api/productos/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		json="{\"id\":3,\"cliente\":{\"id\":3},\"precioTotal\":\"0\"}";
		this.mockMvc.perform(post("/api/pedidos/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		json="{\"id\":3,\"producto\":{\"id\":4},\"pedido\":{\"id\":3}}";
		this.mockMvc.perform(post("/api/productoPedidos/add").contentType(MediaType.APPLICATION_JSON).content(json)).andExpect(status().isOk()).andExpect(content().json(json));
		this.mockMvc.perform(get("/api/productoPedidos/cantidadProductos/3")).andExpect(status().isOk()).andExpect(content().string("1"));
		this.mockMvc.perform(delete("/api/productoPedidos/delete/3")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(delete("/api/pedidos/delete/3")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(delete("/api/productos/delete/4")).andExpect(status().isOk()).andExpect(content().string("true"));
		this.mockMvc.perform(delete("/api/clientes/delete/3")).andExpect(status().isOk()).andExpect(content().string("true"));
	}
}

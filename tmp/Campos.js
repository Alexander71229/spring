function capital(s){
	return s.charAt(0).toUpperCase()+s.slice(1);
}
function generarClase(n){
	var e=document.getElementById(n);
	e.value=generarCuerpoClase(n,e.value);
}
function generarCuerpoClase(n,t){
	var r="public class "+n+" extends ValueObject implements Serializable{\n";
	var encabezado="package oracle.e1.bssv.JP571201.valueobject;\n";
	encabezado+="import oracle.e1.bssvfoundation.base.ValueObject;\n";
	encabezado+="import java.io.Serializable;\n";
	encabezado+="import java.util.Calendar;\n";
	encabezado+="import oracle.e1.bssvfoundation.util.MathNumeric;\n";
	r=encabezado+r;
	var l=t.split('\n');
	var c=[];
	for(var i=0;i<l.length;i++){
		var d=l[i].split(" ");
		if(d.length==2){
			r+='\tprivate '+d[0]+' '+d[1]+';\n';
			c.push(d);
		}
	}
	for(var i=0;i<c.length;i++){
		//r+=c[i][0]+'.'+c[i][1]+'';
		r+='\tpublic void set'+capital(c[i][1])+'('+c[i][0]+' '+c[i][1]+'){\n';
		r+='\t\tthis.'+c[i][1]+'='+c[i][1]+';\n';
		r+='\t}\n';
		r+='\tpublic '+c[i][0]+' get'+capital(c[i][1])+'(){\n';
		r+='\t\treturn this.'+c[i][1]+';\n';
		r+='\t}\n'
	}
	r+="}";
	console.log(r);
	document.getElementById('respuesta').value=r;
	document.getElementById('respuesta').focus();
	return r;
}
function generarAccesos(t){
	var r="";
	var l=t.split('\n');
	for(var i=0;i<l.length;i++){
		var d=l[i].trim().replace(";","").split(" ");
		if(d[0]=='private'||d[0]=='public'||d[0]=='protected'){
			d.splice(0,1);
		}
		if(d.length==2){
			r+='\tpublic void set'+capital(d[1])+'('+d[0]+' '+d[1]+'){\n';
			r+='\t\tthis.'+d[1]+'='+d[1]+';\n';
			r+='\t}\n';
			r+='\tpublic '+d[0]+' get'+capital(d[1])+'(){\n';
			r+='\t\treturn this.'+d[1]+';\n';
			r+='\t}\n'
		}
	}
	return r;
}
function accesos(n){
	var e=document.getElementById(n);
	var r=generarAccesos(e.value);
	e.value=r;
	e.focus();
}